AM_CPPFLAGS = \
	-D_POSIX_C_SOURCE=200809L \
	-D_DEFAULT_SOURCE \
	$(NULL)
AM_CFLAGS = \
	-std=c99 \
	-Wall \
	-Wextra \
	-Werror \
	$(NULL)
ABNFGEN ?= abnfgen

all: test-validate validator

test-validate: validate.c validate.h test-validate.c
	$(CC) -o $@ $(AM_CPPFLAGS) $(CPPFLAGS) $(AM_CFLAGS) $(CFLAGS) $^

check: test-validate check-generated check-generated-obsolete check-generated-utf8
	./test-validate

validator: validate.c validate.h validator.c
	$(CC) -o $@ $(AM_CPPFLAGS) $(CPPFLAGS) $(AM_CFLAGS) $(CFLAGS) $^

check-generated: validator grammar.abnf
	$(ABNFGEN) -d tests -c -n 100000 -s addr-spec grammar.abnf
	./validator tests

check-generated-obsolete: validator grammar.abnf grammar-obsolete.abnf
	$(ABNFGEN) -d tests-obsolete -c -n 100000 -s addr-spec grammar.abnf grammar-obsolete.abnf
	./validator tests-obsolete

check-generated-utf8: validator grammar.abnf grammar-utf8.abnf
	$(ABNFGEN) -d tests-utf8 -c -n 100000 -s addr-spec grammar.abnf grammar-utf8.abnf
	./validator tests-utf8

clean:
	-rm -f test-validate
	-rm -f validator
	-rm -rf tests tests-obsolete tests-utf8

.PHONY: all clean check check-generated check-generated-obsolete check-generated-utf8
