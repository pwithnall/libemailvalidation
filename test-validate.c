/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © Philip Withnall 2016 <philip@tecnocode.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "validate.h"

int
main (int   argc __attribute__((unused)),
      char *argv[] __attribute__((unused)))
{
  const struct {
    const char *address;
    ssize_t address_len;
    ssize_t expected_error_pos_none;
    ssize_t expected_error_pos_disable_obsolete;
    ssize_t expected_error_pos_disable_folding_whitespace;
  } vectors[] = {
    /* https://en.wikipedia.org/wiki/Email_address#Valid_email_addresses */
    { "prettyandsimple@example.com", -1, -1, -1, -1 },
    { "very.common@example.com", -1, -1, -1, -1 },
    { "disposable.style.email.with+symbol@example.com", -1, -1, -1, -1 },
    { "other.email-with-dash@example.com", -1, -1, -1, -1 },
    { "x@example.com", -1, -1, -1, -1 }, /* one-letter local part */
    { "\"much.more unusual\"@example.com", -1, -1, -1, -1 },
    { "\"very.unusual.@.unusual.com\"@example.com", -1, -1, -1, -1 },
    { "\"very.(),:;<>[]\\\".VERY.\\\"very@\\\\ \\\"very\\\".unusual\"@strange.example.com", -1, -1, -1, -1 },
    { "example-indeed@strange-example.com", -1, -1, -1, -1 },
    { "admin@mailserver1", -1, -1, -1, -1 }, /* local domain name with no TLD */
    { "#!$%&'*+-/=?^_`{}|~@example.org", -1, -1, -1, -1 },
    { "\"()<>[]:,;@\\\\\\\"!#$%&'-/=?^_`{}| ~.a\"@example.org", -1, -1, -1, -1 },
    { "\" \"@example.org", -1, -1, -1, -1 }, /* space between the quotes */
    { "example@localhost", -1, -1, -1, -1 }, /* sent from localhost */
    { "example@s.solutions", -1, -1, -1, -1 }, /* see the List of Internet top-level domains */
    { "user@com", -1, -1, -1, -1 },
    { "user@localserver", -1, -1, -1, -1 },
    { "user@[IPv6:2001:db8::1]", -1, -1, -1, -1 },

    /* https://blogs.msdn.microsoft.com/testing123/2009/02/06/email-address-test-cases/ (valid) */
    { "email@domain.com", -1, -1, -1, -1 },
    { "firstname.lastname@domain.com", -1, -1, -1, -1 },
    { "email@subdomain.domain.com", -1, -1, -1, -1 },
    { "firstname+lastname@domain.com", -1, -1, -1, -1 },
    { "email@123.123.123.123", -1, -1, -1, -1 },
    { "email@[123.123.123.123]", -1, -1, -1, -1 },
    { "\"email\"@domain.com", -1, -1, -1, -1 },
    { "1234567890@domain.com", -1, -1, -1, -1 },
    { "email@domain-one.com", -1, -1, -1, -1 },
    { "_______@domain.com", -1, -1, -1, -1 },
    { "email@domain.name", -1, -1, -1, -1 },
    { "email@domain.museum", -1, -1, -1, -1 },
    { "email@domain.co.jp", -1, -1, -1, -1 },
    { "firstname-lastname@domain.com", -1, -1, -1, -1 },

    /* https://blogs.msdn.microsoft.com/testing123/2009/02/06/email-address-test-cases/ (invalid) */
    { "plainaddress", -1, 12, 12, 12 },
    { "#@%^%#$@#$@#.com", -1, 7, 7, 7 },
    { "@domain.com", -1, 0, 0, 0 },
    { "Joe Smith <email@domain.com>", -1, 4, 4, 4 },
    { "email.domain.com", -1, 16, 16, 16 },
    { "email@domain@domain.com", -1, 12, 12, 12 },
    { ".email@domain.com", -1, 0, 0, 0 },
    { "email.@domain.com", -1, 5, 5, 5 },
    { "email..email@domain.com", -1, 5, 5, 5 },
    { "あいうえお@domain.com", -1, -1, -1, -1 },  /* valid since UTF-8 support was added (blog is outdated) */
    { "email@domain.com (Joe Smith)", -1, -1, -1, 17 },  /* brackets count as CFWS comment */
    /* FIXME: { "email@-domain.com", -1, 6, 6, 6 }, */
    { "email@domain..com", -1, 12, 12, 12 },

    /* http://codefool.tumblr.com/post/15288874550/list-of-valid-and-invalid-email-addresses (valid) */
    { "much.\"more\\ unusual\"@domain.com", -1, -1, 4, -1 },
    { "very.unusual.\"@\".unusual.com@domain.com", -1, -1, 12, -1 },
    { "very.\"(),:;<>[]\".VERY.\"very@\\\\\\ \\\"very\".unusual@strange.domain.com", -1, -1, 4, -1 },

    /* http://codefool.tumblr.com/post/15288874550/list-of-valid-and-invalid-email-addresses (invalid) */
    { "Abc..123@domain.com", -1, 3, 3, 3 },
    { "\"(),:;<>[\\]@domain.com", -1, 22, 22, 22 },
    { "just\"not\"right@domain.com", -1, 4, 4, 4 },
    { "this\\ is\\\"really\\\"not\\\\allowed@domain.com", -1, 4, 4, 4 },

    /* Extracted from RFC 3696 using `grep @`; updated for errata
     * 246, 3563 and 4002; but note that RFC 5322 supersedes some of the
     * information in RFC 2822 (and hence RFC 3696, which is based on 2822),
     * and disallows backslash escapes outside comments, domains and quotes */
    { "\"Abc@def\"@example.com", -1, -1, -1, -1 },
    { "\"Fred Bloggs\"@example.com", -1, -1, -1, -1 },
    { "\"Joe.\\\\Blow\"@example.com", -1, -1, -1, -1 },
    { "Joe@example.com", -1, -1, -1, -1 },
    { "user+mailbox@example.com", -1, -1, -1, -1 },
    { "customer/department=shipping@example.com", -1, -1, -1, -1 },
    { "$A12345@example.com", -1, -1, -1, -1 },
    { "!def!xyz%abc@example.com", -1, -1, -1, -1 },
    { "_somename@example.com", -1, -1, -1, -1 },

    /* Extracted from RFC 5321 using `grep @` */
    { "local-part@domain", -1, -1, -1, -1 },
    { "jsmith@foo.com", -1, -1, -1, -1 },
    { "hsmith@foo.com", -1, -1, -1, -1 },
    { "dweep@foo.com", -1, -1, -1, -1 },
    { "Postel@isi.edu", -1, -1, -1, -1 },
    { "Fonebone@physics.foo-u.edu", -1, -1, -1, -1 },
    { "SQSmith@specific.generic.com", -1, -1, -1, -1 },
    { "userx@y.foo.org", -1, -1, -1, -1 },
    { "@hosta.int,@jkl.org:userc@d.bar.org", -1, 0, 0, 0 },
    { "userc@d.bar.org", -1, -1, -1, -1 },
    { "@jkl.org:userc@d.bar.org", -1, 0, 0, 0 },
    { "@a,@b:user@d", -1, 0, 0, 0 },
    { "user@d", -1, -1, -1, -1 },
    { "Smith@bar.com", -1, -1, -1, -1 },
    { "Jones@foo.com", -1, -1, -1, -1 },
    { "Green@foo.com", -1, -1, -1, -1 },
    { "Brown@foo.com", -1, -1, -1, -1 },
    { "JQP@bar.com", -1, -1, -1, -1 },
    { "Jones@XYZ.COM", -1, -1, -1, -1 },
    { "Jones@xyz.com", -1, -1, -1, -1 },
    { "Admin.MRC@foo.com", -1, -1, -1, -1 },
    { "EAK@bar.com", -1, -1, -1, -1 },

    /* Extracted from RFC 5322 using `grep @` */
    { "boss@nil.test", -1, -1, -1, -1 },
    { "c@a.test", -1, -1, -1, -1 },
    /* FIXME: Converted ’ to '; should this be an erratum? */
    { "c@(Chris's host.)public.example", -1, -1, -1, 2 },
    { "j-brown@other.example", -1, -1, -1, -1 },
    { "jdoe@example.org", -1, -1, -1, -1 },
    { "jdoe@machine(comment)", -1, -1, -1, 12 },
    { "jdoe@machine.example", -1, -1, -1, -1 },
    { "jdoe@node.example", -1, -1, -1, -1 },
    { "jdoe@one.test", -1, -1, -1, -1 },
    { "jdoe@test", -1, -1, -1, -1 },
    { "joe@example.org", -1, -1, -1, -1 },
    { "joe@where.test", -1, -1, -1, -1 },
    { "john.q.public@example.com", -1, -1, -1, -1 },
    { "mary@example.net", -1, -1, -1, -1 },
    { "mary@x.test", -1, -1, -1, -1 },
    { "mjones@machine.example", -1, -1, -1, -1 },
    { "@node.test:mary@example.net", -1, 0, 0, 0 },
    { "one@y.test", -1, -1, -1, -1 },
    { "pete(his account)@silly.test(his host)", -1, -1, -1, 4 },
    { "pete@silly.example", -1, -1, -1, -1 },
    { "smith@home.example", -1, -1, -1, -1 },
    { "sysservices@example.net", -1, -1, -1, -1 },
    { "testabcd.1234@silly.test", -1, -1, -1, -1 },

    /* https://www.rfc-editor.org/errata_search.php?rfc=5322&eid=3135 */
    { "\"\"@ietf.org", -1, 1, 1, 1 },
    { "foo.\"\"@ietf.org", -1, 3, 3, 3 },
    { "\"\".bar@ietf.org", -1, 1, 1, 1 },
    { "foo.\"\".bar@ietf.org", -1, 3, 3, 3 },
    { "foo\"\"@ietf.org", -1, 3, 3, 3 },
    { "\"\"bar@ietf.org", -1, 1, 1, 1 },
    { "foo\"\"bar@ietf.org", -1, 3, 3, 3 },

    /* Trivial invalid examples. */
    { "no-at-sign", -1, 10, 10, 10 },
    { "@", -1, 0, 0, 0 },
    { "@hi", -1, 0, 0, 0 },
    { "test@.", -1, 5, 5, 5 },
    { "", -1, 0, 0, 0 },

    /* Trivial examples containing comments. */
    { "hello(comment)@example.com", -1, -1, -1, 5 },
    /* Comments cannot split atoms: */
    { "hel(comment)lo@example.com", -1, 12, 12, 3 },
    { "(comment)hello@example.com", -1, -1, -1, 0 },
    { "hello@(comment)example.com", -1, -1, -1, 6 },
    { "hello@exam(comment)ple.com", -1, 19, 19, 10 },
    /* Comments are only allowed at the beginning and end of domains,
     * unless obsolete syntax is enabled, then they are also allowed
     * at the beginning and end of atoms: */
    { "hello@example(comment).com", -1, -1, 22, 13 },
    { "hello@example.(comment)com", -1, -1, 13, 13 },
    { "hello@example.com(comment)", -1, -1, -1, 17 },
    { "(comment)@example.com", -1, 9, 9, 0 },
    { "hello@(comment).com", -1, 15, 15, 6 },
    { "hello@(comment)", -1, 15, 15, 6 },
    { "hello(comment (nested comment))@example.com", -1, -1, -1, 5 },
    { "hello(comment (nested comment \\(with escaping\\)))@example.com", -1, -1, -1, 5 },
    { "hello()@example.com", -1, -1, -1, 5 },

    /* Length limitations. */
    { "1234567890123456789012345678901234567890123456789012345678901234@example.com", -1, -1, -1, -1 },
    { "12345678901234567890123456789012345678901234567890123456789012341@example.com", -1, 64, 64, 64 },
    { "test@"
      "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij"
      "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij"
      "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij"
      "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij"
      "abcdefghijabcde", -1, -1, -1, -1 },
    { "test@"
      "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij"
      "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij"
      "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij"
      "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij"
      "abcdefghijabcdef", -1, 260, 260, 260 },

    /* Various tests which were generated by abnfgen at some point, and managed
     * to crash the parser. */

    /* This one uses `\0` from obs-qp. That’s an actual nul byte. */
    { "\t\r\n  '.\"!8# \\\000E  \t\t\"@\t\t(\t(\t)\r\n \r\n\t\t\\a )\t \r\n "
      "\r\n (\r\n\t)!_\r\n\t \t\t(\r\n )(\r\n\t).^|~|!", 76, -1, 6, 1 },
  };
  size_t i;

  /* TAP output. */
  printf ("1..%u\n", (unsigned int) (4 * sizeof (vectors) / sizeof (*vectors)));

  for (i = 0; i < sizeof (vectors) / sizeof (*vectors); i++)
    {
      bool is_valid;
      size_t error_pos;
      size_t address_len;

      if (vectors[i].address_len < 0)
        address_len = strlen (vectors[i].address);
      else
        address_len = vectors[i].address_len;

      printf ("# %s %zi %zi %zi\n", vectors[i].address,
              vectors[i].expected_error_pos_none,
              vectors[i].expected_error_pos_disable_obsolete,
              vectors[i].expected_error_pos_disable_folding_whitespace);

      /* EMV_VALIDATE_FLAGS_NONE. */
      is_valid = emv_validate_email_address (vectors[i].address, address_len,
                                             EMV_VALIDATE_FLAGS_NONE,
                                             &error_pos);
      printf ("# none %u %u\n", is_valid, (unsigned int) error_pos);

      assert (is_valid == (vectors[i].expected_error_pos_none == -1));
      assert (error_pos <= address_len);
      assert (vectors[i].expected_error_pos_none < 0 ||
              error_pos == (size_t) vectors[i].expected_error_pos_none);

      printf ("ok %u none %s\n", (unsigned int) (4 * i + 1), vectors[i].address);

      /* EMV_VALIDATE_FLAGS_DISABLE_OBSOLETE. */
      is_valid = emv_validate_email_address (vectors[i].address, address_len,
                                             EMV_VALIDATE_FLAGS_DISABLE_OBSOLETE,
                                             &error_pos);
      printf ("# disable-obsolete %u %u\n", is_valid, (unsigned int) error_pos);

      assert (is_valid == (vectors[i].expected_error_pos_disable_obsolete == -1));
      assert (error_pos <= address_len);
      assert (vectors[i].expected_error_pos_disable_obsolete < 0 ||
              error_pos == (size_t) vectors[i].expected_error_pos_disable_obsolete);

      printf ("ok %u disable-obsolete %s\n",
              (unsigned int) (4 * i + 2), vectors[i].address);

      /* EMV_VALIDATE_FLAGS_DISABLE_FOLDING_WHITESPACE. */
      is_valid = emv_validate_email_address (vectors[i].address, address_len,
                                             EMV_VALIDATE_FLAGS_DISABLE_FOLDING_WHITESPACE,
                                             &error_pos);
      printf ("# disable-folding-whitespace %u %u\n",
              is_valid, (unsigned int) error_pos);

      assert (is_valid == (vectors[i].expected_error_pos_disable_folding_whitespace == -1));
      assert (error_pos <= address_len);
      assert (vectors[i].expected_error_pos_disable_folding_whitespace < 0 ||
              error_pos == (size_t) vectors[i].expected_error_pos_disable_folding_whitespace);

      printf ("ok %u disable-folding-whitespace %s\n",
              (unsigned int) (4 * i + 3), vectors[i].address);

      /* EMV_VALIDATE_FLAGS_DISABLE_OBSOLETE and
       * EMV_VALIDATE_FLAGS_DISABLE_FOLDING_WHITESPACE. */
      is_valid = emv_validate_email_address (vectors[i].address, address_len,
                                             EMV_VALIDATE_FLAGS_DISABLE_OBSOLETE |
                                             EMV_VALIDATE_FLAGS_DISABLE_FOLDING_WHITESPACE,
                                             &error_pos);
      printf ("# disable-obsolete-and-folding-whitespace %u %u\n",
              is_valid, (unsigned int) error_pos);

      assert (is_valid ==
              (vectors[i].expected_error_pos_disable_obsolete == -1 &&
               vectors[i].expected_error_pos_disable_folding_whitespace == -1));
      assert (error_pos <= address_len);
      assert ((vectors[i].expected_error_pos_disable_obsolete < 0 ||
               error_pos == (size_t) vectors[i].expected_error_pos_disable_obsolete) ||
              (vectors[i].expected_error_pos_disable_folding_whitespace < 0 ||
               error_pos == (size_t) vectors[i].expected_error_pos_disable_folding_whitespace));

      printf ("ok %u disable-obsolete-and-folding-whitespace %s\n",
              (unsigned int) (4 * i + 4), vectors[i].address);
    }

  return 0;
}
