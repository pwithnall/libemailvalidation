/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © Philip Withnall 2016 <philip@tecnocode.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EMV_VALIDATE_H
#define EMV_VALIDATE_H

#include <stdbool.h>
#include <stdlib.h>

/**
 * EmvValidateFlags:
 * @EMV_VALIDATE_FLAGS_NONE: Default; enable no special validation options.
 * @EMV_VALIDATE_FLAGS_DISABLE_OBSOLETE: Disable accepting obsolete e-mail
 *    address syntax, as specified in
 *    [RFC 5322, §4](https://tools.ietf.org/html/rfc5322#section-4).
 * @EMV_VALIDATE_FLAGS_DISABLE_FOLDING_WHITESPACE: Disable accepting folding
 *    whitespace, as specified in
 *    [RFC 5322, §2.2.3](https://tools.ietf.org/html/rfc5322#section-2.2.3).
 * @EMV_VALIDATE_FLAGS_DISABLE_UTF8: Disable accepting internationalised e-mail
 *    addresses via UTF-8, as specified in
 *    [RFC 6532](https://tools.ietf.org/html/rfc6532).
 * @EMV_VALIDATE_FLAGS_DISABLE_LENGTH: Disable length checks on e-mail address
 *    local parts and domain names, as summarised in
 *    [RFC 3696, §3](https://tools.ietf.org/html/rfc3696#section-3).
 *
 * Option flags which affect the syntax accepted during validation with
 * emv_validate_email_address().
 *
 * Since: UNRELEASED
 */
typedef enum
{
  EMV_VALIDATE_FLAGS_NONE = 0,
  EMV_VALIDATE_FLAGS_DISABLE_OBSOLETE = (1 << 0),
  EMV_VALIDATE_FLAGS_DISABLE_FOLDING_WHITESPACE = (1 << 1),
  EMV_VALIDATE_FLAGS_DISABLE_UTF8 = (1 << 2),
  EMV_VALIDATE_FLAGS_DISABLE_LENGTH = (1 << 3),
} EmvValidateFlags;

bool emv_validate_email_address (const char       *address,
                                 ssize_t           address_len,
                                 EmvValidateFlags  flags,
                                 size_t           *error_pos);

#endif /* !EMV_VALIDATE_H */
