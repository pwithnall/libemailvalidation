/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © Philip Withnall 2016 <philip@tecnocode.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "validate.h"

static void
fatal (const char *format,
       ...) __attribute__ ((format (printf, 1, 2)));

static void
fatal (const char *format,
       ...)
{
  va_list args;

  va_start (args, format);
  vfprintf (stderr, format, args);
  va_end (args);

  raise (SIGTRAP);
  exit (2);
}

static int
syscall_0_or_fatal (int retval)
{
  if (retval != 0)
    fatal ("Syscall failed: %s\n", strerror (errno));
  return retval;
}

static int
syscall_gte_0_or_fatal (int retval)
{
  if (retval < 0)
    fatal ("Syscall failed: %s\n", strerror (errno));
  return retval;
}

static void *
syscall_nonnull_or_fatal (void *retval)
{
  if (retval == NULL)
    fatal ("Syscall failed: %s\n", strerror (errno));
  return retval;
}

static void
validate_file (int           dir_fd,
               const char   *filename,
               unsigned int  idx)
{
  int fd;
  ssize_t n_read;
  char buf[4096];
  bool success;

  /* At the moment we only support a single-buffer read. */
  fd = syscall_gte_0_or_fatal (openat (dir_fd, filename, O_RDONLY | O_CLOEXEC));
  n_read = syscall_gte_0_or_fatal (read (fd, buf, sizeof (buf)));
  assert (n_read > 0 && (size_t) n_read < sizeof (buf));
  close (fd);

  buf[n_read] = 0;
  success = emv_validate_email_address (buf, n_read,
                                        EMV_VALIDATE_FLAGS_DISABLE_LENGTH,
                                        NULL);

  printf ("%s %u none %s\n", success ? "ok" : "not ok", idx, filename);
}

int
main (int   argc,
      char *argv[])
{
  struct stat stat_buf;

  /* Accept a single file or directory name to validate. */
  if (argc != 2)
    {
      fprintf (stderr, "Usage: %s filename_or_directory_name\n", argv[0]);
      return 1;
    }

  syscall_0_or_fatal (stat (argv[1], &stat_buf));

  if (stat_buf.st_mode & S_IFREG)
    {
      /* Reading a single file. */
      printf ("1..1\n");
      validate_file (AT_FDCWD, argv[1], 0);
    }
  else if (stat_buf.st_mode & S_IFDIR)
    {
      unsigned int i, n_tests;
      struct dirent *dir_entry;
      DIR *dir;

      /* Reading every file in the directory. */
      dir = syscall_nonnull_or_fatal (opendir (argv[1]));

      /* Work out how many files are in the directory, for TAP output. */
      for (n_tests = 0, dir_entry = readdir (dir);
           dir_entry != NULL;
           dir_entry = readdir (dir))
        {
          if (dir_entry->d_type != DT_REG)
            continue;

          n_tests++;
          assert (dir_entry->d_type != DT_UNKNOWN);
        }
      rewinddir (dir);

      printf ("1..%u\n", n_tests);

      for (i = 0, dir_entry = readdir (dir);
           dir_entry != NULL;
           dir_entry = readdir (dir))
        {
          if (dir_entry->d_type != DT_REG)
            continue;

          i++;
          validate_file (dirfd (dir), dir_entry->d_name, i);
        }

      assert (i == n_tests && dir_entry == NULL);

      closedir (dir);
    }
  else
    {
      fprintf (stderr, "%s: Argument must be a filename or directory name\n",
               argv[0]);
      return 2;
    }

  return 0;
}
